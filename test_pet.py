
from libs.pet_libs import PetLibs
#class
#function
#encapsulation
#object
#constructor

#inheritance

class TestPet(PetLibs):
    # Class is a collection of functions\methods and variables
    base_url = "https://petstore.swagger.io/v2/pet/"
    GET_PET_BY_STATUS = "findByStatus?status={}"
    GET_PET_BY_TAGS = "findByTags?tags={}"

    # Consructor
    def __init__(self,name,address):
        print("in constructor {} {}",name,address)
        self.name=name
        self.address=address

    def test_get_pet_by_status(self,status):
        '''
        :param status: Status needs to validate
        :return: nothing
        '''

        print("I am in test method")
        resp=self.get_pet(url=self.base_url+self.GET_PET_BY_STATUS.format(status))
        for record in resp:
            assert record[
                       'status'] == status, "status is not correct for id {} status should be available but showing {}".format(
                record["id"], record["status"])

    def test_pet_by_tags(self,tag_name):
        print("I am in test method")
        resp=self.get_pet(url=self.base_url+self.GET_PET_BY_TAGS.format(tag_name))
        for record in resp:
            assert record[
                       'tags'][0]['name'] == tag_name, "status is not correct for id {} tag should be {} but showing {}".format(
                tag_name,record["id"], record["tags"][0]['name'])




    def add_nos(self,a=2,b=2):
        print("value of a={}".format(a))
        print("value of b={}".format(b))
        c=a+b
        print(self.address)
        print(self.name)
        return c



ob=TestPet("Test","Pune")

print(ob.add_nos())
print(ob.add_nos(5,8))
print(ob.add_nos(9,12))

ob.test_get_pet_by_status("available")
ob.test_pet_by_tags("string")

#print(ob.base_url)

